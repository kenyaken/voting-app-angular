import { CurrencyPipe } from '@angular/common';
import { Component,  OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-poll',
  templateUrl: './poll.component.html',
  styleUrls: ['./poll.component.css']
})
export class PollComponent implements OnInit {
  // @Input decorator used to manipulate the child
  // component from the parent i.e
  @Input() question: string;
  @Input() votes: number[];
  @Input() voted: boolean;
  @Input() pollImage: string;

  //local variable
  numberOfVotes: number;
  constructor() {

  }

  ngOnInit(): void {
        if(this.votes.length){
      this.numberOfVotes = this.votes.reduce((acc,curr) => {
        return acc += curr;
      })
    }
  }

}
