import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Poll, PollForm } from '../type';

@Injectable({
  providedIn: 'root'
})
export class PollService {

  constructor() { }

  getPolls(): Observable<Poll[]>{
    return of(
      [{
    id: 2,
    question: 'Do you support BBI?',
    thumbnail:"",
    results: [15,3,2],
    options: ["No","Yes","Cannot Vote!"],
    voted: true,
  }, {
    id: 4,
    question: 'Which team do you support',
    thumbnail:"",
    results: [22,18,40],
    options: ["Man City","Arsenal","Liverpool"],
    voted: false,
  }

]
    ).pipe(delay(2000));
  }

  vote(pollId:number, voteNumber: number){
    console.log(pollId,voteNumber);
  }

  createPoll(poll: PollForm){
    console.log(poll);
  }
}
