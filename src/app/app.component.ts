import { Component } from '@angular/core';
import { PollService } from './poll-service/poll.service';
import { Poll, PollForm, PollVote } from './type';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  showForm = false;

  activePoll:Poll;
  polls = this.ps.getPolls();

  constructor(private ps: PollService){

  }


  setActivePoll(poll:Poll){

  this.activePoll = null;

  setTimeout(() => {
    this.activePoll=poll;
  },100)
  }
  handlePollCreated(poll: PollForm){
  this.ps.createPoll(poll);
  }
  handlePollVoted(pollVoted:PollVote){
    this.ps.vote(pollVoted.id, pollVoted.vote)
  }

}
